﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp12
{
    public partial class lvl4 : Form
    {
        public lvl4()
        {
            InitializeComponent();
        }

        private void lvl4_Load(object sender, EventArgs e)
        {
            button16.BackColor = Color.White;
            button10.Size = new Size(51, 150);
            button9.Size = new Size(51, 150);
            button8.Size = new Size(51, 150);
            button7.Size = new Size(51, 150);
        }

        private void button11_MouseEnter(object sender, EventArgs e)
        {
          
        }

        private void button12_MouseEnter(object sender, EventArgs e)
        {
            button7.Size = new Size(137, 30);

        }

        private void button13_MouseEnter(object sender, EventArgs e)
        {
 button8.Size = new Size(51, 336);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            Cursor.Position = button11.PointToScreen(new Point(0, 0));
        }

        private void button11_MouseEnter_1(object sender, EventArgs e)
        {
  button10.Size = new Size(51, 343);
        }

        private void lvl4_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Cursor.Clip = base.Bounds;
            base.TopMost = true;
            base.WindowState = FormWindowState.Normal;
        }

        private void button15_MouseEnter(object sender, EventArgs e)
        {
            timer1.Stop();
            lvl5 lvl5 = new lvl5();
            timer1.Stop();
            lvl5.Show();
            timer1.Stop();
            this.Hide();
        }

        private void button12_MouseEnter_1(object sender, EventArgs e)
        {
            button11.BackColor = Color.Black;
        }
    }
}
