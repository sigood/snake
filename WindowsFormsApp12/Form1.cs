﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace WindowsFormsApp12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button6_MouseEnter(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Position = button10.PointToScreen(new Point(0, 0));
        }

        private void button8_MouseEnter(object sender, EventArgs e)
        {
            timer1.Stop();
            lvl2 l2 = new lvl2();
            l2.Show();
            this.Visible = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          //  MessageBox.Show(button1.PointToScreen(new Point(0, 0)).ToString());
            Cursor.Clip = new Rectangle(this.Location, this.Size);
           
            //  button8.Visible = false;
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
          

        }

        private void button9_Click(object sender, EventArgs e)
        {
            string caseSwitch = textBox1.Text;
            switch (caseSwitch)
            {
                case "aa3":
                    timer1.Stop();
                    Author aa3 = new Author();
                    aa3.Show();
                    this.Visible = false;
                    break;
                case "l5":
                    timer1.Stop();
                    lvl5 l5 = new lvl5();
                    l5.Show();
                    this.Visible = false;
                    break;
                case "l4":
                    timer1.Stop();
                    lvl4 l4 = new lvl4();
                    l4.Show();
                    this.Visible = false;
                    break;
                case "l3":
                    timer1.Stop();
                    lvl3 l3 = new lvl3();
                    l3.Show();
                    this.Visible = false;
                    break;
                case "l2":
                    timer1.Stop();
                    lvl2 l2 = new lvl2();
                    l2.Show();
                    this.Visible = false;
                    break;
                default:
                    Console.WriteLine("Эх..");
                    break;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Cursor.Clip = base.Bounds;
            base.TopMost = true;
            base.WindowState = FormWindowState.Normal;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
