﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp12
{
    public partial class lvl2 : Form
    {
        public lvl2()
        {
            InitializeComponent();
        }

        private void lvl2_Load(object sender, EventArgs e)
        {
            Cursor.Clip = new Rectangle(this.Location, this.Size);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Position = button20.PointToScreen(new Point(0, 0));

        }
        //int dx = this.Location.X;
        //int dy = this.Location.Y;
        //Cursor.Position = new Point(x + dx, y + dy); // x, y - прочитанные координаты относительно 0 формы

        private void button19_MouseEnter(object sender, EventArgs e)
        {
            timer1.Stop();
            lvl3 lvl3 = new lvl3();
            lvl3.Show();
            this.Hide();      
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Cursor.Clip = base.Bounds;
            base.TopMost = true;
            base.WindowState = FormWindowState.Normal;
        }

        private void lvl2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}