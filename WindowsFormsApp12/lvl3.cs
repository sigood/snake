﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp12
{
    public partial class lvl3 : Form
    {
        public lvl3()
        {
            InitializeComponent();
        }

        private void button26_MouseEnter(object sender, EventArgs e)
        {
            button27.Visible = true;
        }

        private void button31_MouseEnter(object sender, EventArgs e)
        {
            button32.Visible = true;
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Position = button34.PointToScreen(new Point(0, 0));
        }

        private void button33_MouseEnter(object sender, EventArgs e)
        {
            
            lvl4 lvl4 = new lvl4();
            timer1.Stop();
            lvl4.Show();
            timer1.Stop();
            this.Hide();
            timer1.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Cursor.Clip = base.Bounds;
            base.TopMost = true;
            base.WindowState = FormWindowState.Normal;
        }

        private void lvl3_Load(object sender, EventArgs e)
        {
            Cursor.Clip = new Rectangle(this.Location, this.Size);
        }

        private void lvl3_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
